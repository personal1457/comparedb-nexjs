import axios from "axios";

export const getMysql = (max = 2) => {
  return axios.get(`/api/sql/getdata?limit=${max}`);
};

export const getNosql = (max = 2) => {
  return axios.get(`/api/nosql/getdata?limit=${max}`);
};

export const postNosql = (reqBody, dataCount) => {
  return axios.post(`/api/nosql/postdata?count=${dataCount}`, reqBody);
};

export const postSql = (reqBody) => {
  return axios.post(`/api/sql/postdata`, reqBody);
};

export const updateSql = (max, reqBody) => {
  return axios.put(`/api/sql/updatedata?limit=${max}`, { variant: reqBody });
};

export const updateNoSql = (max, reqBody) => {
  return axios.put(`/api/nosql/updatedata?limit=${max}`, { variant: reqBody });
};

export const deleteSql = (max) => {
  return axios.delete(`/api/sql/deletedata?limit=${max}`);
};

export const deleteNoSql = (max) => {
  return axios.delete(`/api/nosql/deletedata?limit=${max}`);
};
