"use client";
import styles from "./page.module.css";
import { useState } from "react";
import {
  deleteNoSql,
  deleteSql,
  getMysql,
  getNosql,
  postNosql,
  postSql,
  updateNoSql,
  updateSql,
} from "./request";
import dataSet from "../sample.json";

export default function Home() {
  const [databaseType, setDatabaseType] = useState("sql");
  const [requestType, setRequestType] = useState("GET");
  const [dataUpdate, setDataUpdate] = useState("");
  const [limitData, setLimitData] = useState(0);
  const [resultTime, setResultTime] = useState(0);

  // handle timer
  let start = "";
  let stop = "";
  const startTimer = () => {
    start = performance.now().toFixed();
  };

  const endTimer = () => {
    stop = performance.now().toFixed();
    const result = stop - start;
    setResultTime(result);
  };

  const handlePostData = async () => {
    if (!dataSet) {
      return alert("Data set kosong !!");
    }

    startTimer();
    for (let count = 1; count <= limitData; count++) {
      if (databaseType === "sql") {
        await postSql(dataSet[count]).then((res) =>
          console.log(res.data.status)
        );
      } else {
        let loopCount = count;

        if (count == limitData) {
          loopCount = "last";
        }

        await postNosql(dataSet[count], loopCount).then((res) =>
          console.log(res.data.status)
        );
      }
    }
    endTimer();
  };

  const handleGetData = async () => {
    startTimer();
    if (databaseType === "sql") {
      await getMysql(limitData).then((res) => console.log(res.data.status));
    } else {
      await getNosql(limitData)
        .then((res) => console.log(res.data.status))
        .catch((error) => console.log(error));
    }
    endTimer();
  };

  const handleUpdateData = async () => {
    startTimer();
    if (databaseType === "sql") {
      await updateSql(limitData, dataUpdate).then((res) =>
        console.log(res.data.status)
      );
    } else {
      await updateNoSql(limitData, dataUpdate).then((res) =>
        console.log(res.data.status)
      );
    }
    endTimer();
  };

  const handleDeleteData = async () => {
    startTimer();
    if (databaseType === "sql") {
      await deleteSql(limitData).then((res) => console.log(res.data.status));
    } else {
      await deleteNoSql(limitData).then((res) => console.log(res.data.status));
    }
    endTimer();
  };

  const handleOnSubmitReq = () => {
    setResultTime("Loading...");

    if (requestType === "GET") {
      return handleGetData();
    }

    if (requestType === "POST") {
      return handlePostData();
    }

    if (requestType === "PUT") {
      return handleUpdateData();
    }

    return handleDeleteData();
  };

  return (
    <main className={styles.main}>
      <div className={styles.center}>
        <div className={styles.flex}>
          {/* Database Type */}
          <div className={styles.card}>
            <h4 className={styles.title}>Database Type</h4>
            <select
              className={styles.drop_down}
              onChange={(e) => setDatabaseType(e.target.value)}
            >
              <option value="sql">SQL</option>
              <option value="nosql">No SQL</option>
            </select>
          </div>

          {/* Request Type */}
          <div className={styles.card}>
            <h4 className={styles.title}>Request Type</h4>
            <select
              className={styles.drop_down}
              onChange={(e) => setRequestType(e.target.value)}
            >
              <option value="GET">GET</option>
              <option value="POST">POST</option>
              <option value="PUT">UPDATE</option>
              <option value="DELETE">DELETE</option>
            </select>
          </div>

          {
            /* Data update update */
            requestType === "PUT" ? (
              <div className={styles.card}>
                <h4 className={styles.title}>New data </h4>
                <input
                  type="text"
                  className={styles.drop_down}
                  onChange={(e) => setDataUpdate(e.target.value)}
                />
              </div>
            ) : null
          }

          {/* Limit update */}
          <div className={styles.card}>
            <h4 className={styles.title}>Limit Data</h4>
            <input
              type="number"
              className={styles.drop_down}
              onChange={(e) => setLimitData(e.target.value)}
            />
          </div>
        </div>

        {/* Submit Button */}
        <button className={styles.button} onClick={handleOnSubmitReq}>
          Submit
        </button>

        <h1 style={{ color: "black", marginTop: "90px" }}>
          Time: {resultTime} ms
        </h1>
      </div>
      {requestType === "POST" ? (
        <div style={{ display: "flex" }}>
          <h1 style={{ color: "black" }}>Data Set: </h1>
          <h1 style={{ color: "black" }}> {dataSet.length} </h1>
        </div>
      ) : null}
    </main>
  );
}
