import mysql from "mysql2/promise";
import { MongoClient } from "mongodb";

export const createMysqlConnection = () =>
  mysql.createConnection({
    host: "localhost",
    database: "travel",
    port: "3307",
    user: "root",
    password: "admin",
  });

export const createMongoConnection = async () => {
  const client = new MongoClient("mongodb://admin:admin@localhost:27018/admin");
  await client.connect();

  return client.db();
};
