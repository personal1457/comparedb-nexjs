import { createMysqlConnection } from "@/pages/connection/connectionModule";

export default async function handler(req, res) {
  if (req.method !== "PUT") {
    return res.status(200).json({ error: "Method invalid" });
  }

  const body = req.body;
  const params = req.query;
  const dbConnection = await createMysqlConnection();

  // variant => nama column
  const query = `UPDATE product SET variant = '${body.variant}' LIMIT ${params.limit}`;

  dbConnection
    .execute(query)
    .then(() => {
      return res.status(200).json({ status: "Success updated" });
    })
    .catch((error) => {
      return res.status(201).json({ failed: error });
    })
    .finally(() => {
      dbConnection.end();
    });
}
