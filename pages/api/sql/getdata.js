import { createMysqlConnection } from "@/pages/connection/connectionModule";

export default async function handler(req, res) {
  const dbConnection = await createMysqlConnection();
  const params = req.query;
  try {
    const query = `SELECT * FROM product LIMIT ${params.limit}`;
    const values = [];
    const [data] = await dbConnection.execute(query, values);
    dbConnection.end();

    res.status(200).json({ result: data });
  } catch (error) {
    res.status(200).json({ errors: error });
  }
}
