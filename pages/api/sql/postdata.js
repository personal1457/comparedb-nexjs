import { createMysqlConnection } from "@/pages/connection/connectionModule";

export default async function handler(req, res) {
  if (req.method !== "POST") {
    return res.status(200).json({ error: "Method salah" });
  }

  let item = req.body;
  const dbConnection = await createMysqlConnection();

  const query = `INSERT INTO product VALUES 
  ('${item.id}', '${item.case_id}', '${item.activity}', '${item.resource}', 
    '${item.complete_timestamp}','${item.variant}', ${item.variant_index}, '${item.declarationnumber}', '${item.amount}','${item.requestedamount}','${item.originalamount}', '${item.budgetnumber}', '${item.adjustedamount}', '${item["org:role"]}','${item.permit_id}')`;

  dbConnection
    .execute(query)
    .then(() => {
      return res.status(200).json({ status: "Success" });
    })
    .catch((error) => {
      return res.status(201).json({ failed: error });
    })
    .finally(() => {
      dbConnection.end();
    });
}
