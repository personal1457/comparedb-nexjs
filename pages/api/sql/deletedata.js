import { createMysqlConnection } from "@/pages/connection/connectionModule";

export default async function handler(req, res) {
  if (req.method !== "DELETE") {
    return res.status(200).json({ error: "Method salah" });
  }

  const params = req.query;
  const dbConnection = await createMysqlConnection();

  const query = `DELETE from product LIMIT ${params.limit}`;

  dbConnection
    .execute(query)
    .then(() => {
      return res.status(200).json({ status: "Success deleted" });
    })
    .catch((error) => {
      return res.status(201).json({ failed: error });
    })
    .finally(() => {
      dbConnection.end();
    });
}
