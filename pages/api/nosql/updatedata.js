import { createMongoConnection } from "@/pages/connection/connectionModule";

export default async function handler(req, res) {
  if (req.method !== "PUT") {
    return res.status(200).json({ error: "Method salah" });
  }
  const params = req.query;
  const body = req.body;
  const db = await createMongoConnection();
  const dataWillBeUpdate = await db
    .collection("test")
    .find()
    .limit(+params.limit)
    .toArray();
  const updateDataIds = dataWillBeUpdate.map((item) => item._id);
  const queryUpdate = { $set: body };

  try {
    await db
      .collection("test")
      .updateMany({ _id: { $in: updateDataIds } }, queryUpdate);

    return res.status(200).json({ status: "Update Success" });
  } catch (error) {
    return res.status(200).json({ status: "Error", message: error });
  }
}
