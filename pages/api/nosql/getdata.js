const {
  createMongoConnection,
} = require("@/pages/connection/connectionModule");

export default async function handler(req, res) {
  const db = await createMongoConnection();
  const params = req.query;
  const collection = db.collection("test");
  const data = await collection.find({}).limit(+params.limit).toArray();

  res.status(201).json(data);
}
