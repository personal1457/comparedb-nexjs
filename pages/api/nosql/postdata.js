import { createMongoConnection } from "@/pages/connection/connectionModule";

let db;
let bulk;

const initConnection = async () => {
  db = await createMongoConnection();
  bulk = db.collection("test").initializeUnorderedBulkOp();
};

const handleInsertBulk = (data) => {
  const parseData = JSON.stringify(data);

  bulk.insert(JSON.parse(parseData));
};

export default async function handler(req, res) {
  const params = req.query;

  if (req.method !== "POST") {
    return res.status(200).json({ error: "Method salah" });
  }

  if (!req.body) {
    res.status(200).json({ error: "Data Request kosong", data: req.body });
  }

  if (params.count === "1") {
    await initConnection();
    handleInsertBulk(req.body);
    return res.status(200).json({ status: "Insert bulk Success" });
  }

  if (params.count !== "last") {
    handleInsertBulk(req.body);
    return res.status(200).json({ status: "Insert bulk Success" });
  }

  try {
    handleInsertBulk(req.body);

    await bulk.execute();

    return res.status(200).json({ status: "Insert Mongo Success" });
  } catch (error) {
    return res.status(200).json({ status: error });
  }
}
