import { createMongoConnection } from "@/pages/connection/connectionModule";

export default async function handler(req, res) {
  if (req.method !== "DELETE") {
    return res.status(200).json({ error: "Method salah" });
  }
  const params = req.query;
  const db = await createMongoConnection();
  const dataWillBeDelete = await db
    .collection("test")
    .find()
    .limit(+params.limit)
    .toArray();
  const deleteDataIds = dataWillBeDelete.map((item) => item._id);

  try {
    await db.collection("test").deleteMany({ _id: { $in: deleteDataIds } });

    return res.status(200).json({ status: "Delete Success" });
  } catch (error) {
    return res.status(200).json({ status: "Error", message: error });
  }
}
